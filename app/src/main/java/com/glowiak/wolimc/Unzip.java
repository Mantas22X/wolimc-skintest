package com.glowiak.wolimc;

// Code based on https://www.delftstack.com/howto/java/java-unzip/
// They should know that it's not good to name variables uppercase.

/* Usage:
 * import com.glowiak.wolimc.Unzip;
 * 
 * Unzip.unzip("/path/to/file.zip", "/path/to/directory");
 */

import com.glowiak.wolimc.config.Config;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Unzip
{
    private static final int BUFFER_SIZE = 4096;
    public static void unzip(String zfp, String dfp) throws IOException {
        File dest = new File(dfp);
        if (!dest.exists()) {
            dest.mkdir();
        }
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zfp));
        ZipEntry ze = zis.getNextEntry();

        while (ze != null) {
            String File_Path = dfp + File.separator + ze.getName();
            if (!ze.isDirectory()) {
                extractFile(zis, File_Path);
            } else {
                File directory = new File(File_Path);
                directory.mkdirs();
            }
            zis.closeEntry();
            ze = zis.getNextEntry();
        }
        zis.close();
    }

    public static void extractFile(ZipInputStream zis, String path) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path));
        byte[] bytes = new byte[BUFFER_SIZE];
        int rb = 0;
        while ((rb = zis.read(bytes)) != -1) {
            bos.write(bytes, 0, rb);
        }
        bos.close();
    }
}
