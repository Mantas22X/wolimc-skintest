package com.glowiak.wolimc;

import com.glowiak.wolimc.DownloadFile;
import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.launch.Classpath;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.launch.Launch;
import com.glowiak.wolimc.gui.MainWindow;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

public class WoliMC
{
    public static void main(String[] args)
    {
        if (args.length <= 0) { MainWindow.run(); } else {
        switch(args[0])
        {
            case "createConfig":
                Config.createNewConfig();
                break;
            case "setGlobalOpt":
                if (args.length < 3) { System.out.println("Not enough arguments!"); System.exit(1); }
                new Config().setConfig(args[1], args[2]);
                break;
            case "guidedConfig":
                guidedConfig();
                break;
            case "createInstance":
                if (args.length < 3) { System.out.println("Not enough arguments!"); System.exit(1); }
                if (!new File(String.format("%s/instances", Config.getCwd())).exists())
                { Config.mkdir(String.format("%s/instances", Config.getCwd())); }
                Instance.createInstance(args[1], args[2]);
                break;
            case "listInstances":
                System.out.println(Instance.getInstanceList());
                break;
            case "deleteInstance":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                Instance.deleteInstance(args[1]);
                break;
            case "downloadJars":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                if (Instance.exists(args[1]))
                { Classpath.fetchJars(args[1]); } else
                { System.out.println("Invalid instance name."); System.exit(1); }
                break;
            case "updateManifest":
                Version.updateVersionManifest();
                break;
            case "select":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                Config.loadConfig();
                if (Instance.exists(args[1]))
                { Config.setConfig("selinst", args[1]); } else
                { System.out.println("Invalid instance name."); System.exit(1); }
                break;
            case "launch":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                Config.loadConfig();
                if (Instance.exists(args[1]))
                { Launch.startInstance(args[1]); } else
                { System.out.println("Invalid instance name."); System.exit(1); }
                break;
            case "setProp":
                if (args.length < 4) { System.out.println("Not enough arguments!"); System.exit(1); }
                if (Instance.exists(args[1]))
                { Instance.setProperty(args[1], args[2], args[3]); } else
                { System.out.println("Invalid instance name."); System.exit(1); }
                break;
            case "help":
                printHelp();
                break;
        } }
    }
    public static void printHelp()
    {
        System.out.println("WoliMC is a java rewrite of PolyMC");
        System.out.println("Usage: $JAVA /path/to/WoliMC.jar $OPT");
        System.out.println("Options:");
        System.out.println("createConfig <null> - create a default config file");
        System.out.println("setGlobalOpt <id> <value> - set option");
        System.out.println("guidedConfig <null> - guided configurator");
        System.out.println("createInstance <name> <version> - create a new instance");
        System.out.println("deleteInstance <name> - remove an instance");
        System.out.println("listInstances <null> - list instances");
        System.out.println("downloadJars <name> - download jars for selected instance");
        System.out.println("updateManifest <null> - update version manifest");
        System.out.println("select <name> - select an instance");
        System.out.println("launch <name> - launch an instance");
        System.out.println("setProp <name> <setting> <value> - modify instance settings");
    }
    public static void guidedConfig()
    {
        if (new File(String.format("%s/wolimc.cfg", Config.getCwd())).exists()) { new File(String.format("%s/wolimc.cfg", Config.getCwd())).delete(); }
        
        try {
            FileWriter fw = new FileWriter(String.format("%s/wolimc.cfg", Config.getCwd()));
            fw.write("This is WoliMC config file\n");
            System.out.print("Java path: ");
            String readm_java = new Scanner(System.in).next();
            fw.write(String.format("%s\n", readm_java));
        
            System.out.print("Memory: ");
            String readm_mem = new Scanner(System.in).next();
            fw.write(String.format("%s\n", readm_mem));
            
            System.out.print("Meta server: ");
            String readm_meta = new Scanner(System.in).next();
            fw.write(String.format("%s\n", readm_meta));
            
            fw.write("default\n");
            
            System.out.print("Nick: ");
            String readm_nick = new Scanner(System.in).next();
            fw.write(String.format("%s\n", readm_nick));
            
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
}
