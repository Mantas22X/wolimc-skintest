package com.glowiak.wolimc.config;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URISyntaxException;
import java.net.URL;

public class Config
{
    public static int WoliMC_VERSION_MAJOR = 0;
    public static int WoliMC_VERSION_MINOR = 0;
    public static int WoliMC_VERSION_HOTFIX = 5;
    public static String WoliMC_CODE_BRANCH = "official";
    
    public static String setting_java;
    public static String setting_mem;
    public static String setting_meta;
    public static String setting_selinst;
    public static String setting_nick;
    public static int kat_kommander = 0;
    
    public static String uses_lwjgl2 = "1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12";
    public static String uses_lwjgl3 = "1.13 1.14 1.15 1.16 1.17 1.18 1.19";
        
    public static String getCwd()
    {
        String w = "";
        try {
            w = new File(".").getCanonicalPath().toString();
        } catch (IOException ioe) { System.out.println(ioe); }
        
        return w;
    }
    public static void mkdir(String dir)
    {
        System.out.println(String.format("Created directory %s", dir));
        new File(dir).mkdir();
    }
    public static void createNewConfig()
    {
        try {
            FileWriter fw = new FileWriter(String.format("%s/wolimc.cfg", getCwd()));
            if (!new File(String.format("%s/instances", getCwd())).exists()) { mkdir(String.format("%s/instances", getCwd())); }
            
            fw.write("This is WoliMC config file\n");
            fw.write("java\n");
            fw.write("256M\n");
            fw.write("https://codeberg.org/glowiak/wolimc/raw/branch/meta\n");
            fw.write("default\n");
            fw.write("Player\n");
            fw.close();
            
            fw = new FileWriter(String.format("%s/kat", getCwd()));
            fw.write("0\n");
            fw.close();
            
            fw = new FileWriter(String.format("%s/v3_act", getCwd()));
            fw.write("128665-85552564-6133456-2539437-9826-367567");
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }  
    }
    public static void loadConfig()
    {
        if (!new File(String.format("%s/wolimc.cfg", getCwd())).exists()) { createNewConfig(); }        
        try {
            FileReader fr = new FileReader(String.format("%s/wolimc.cfg", getCwd()));
            BufferedReader br = new BufferedReader(fr);
            br.readLine();
            setting_java = br.readLine();
            setting_mem = br.readLine();
            setting_meta = br.readLine();
            setting_selinst = br.readLine();
            setting_nick = br.readLine();
            
            br.close();
            fr.close();
            
            fr = new FileReader(String.format("%s/kat", getCwd()));
            br = new BufferedReader(fr);
            
            kat_kommander = Integer.parseInt(br.readLine());
            br.close();
            fr.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static int getSettingLine(String setting)
    {
        int wv = 1;
        switch(setting)
        {
            case "java":
                wv = 1;
                break;
            case "mem":
                wv = 2;
                break;
            case "meta":
                wv = 3;
                break;
            case "selinst":
                wv = 4;
                break;
            case "nick":
                wv = 5;
                break;
        }
        return wv;
    }
    public static String getSettingString(int line)
    {
        String wv = "";
        switch(line)
        {
            case 1:
                wv = "java";
                break;
            case 2:
                wv = "mem";
                break;
            case 3:
                wv = "meta";
                break;
            case 4:
                wv = "selinst";
                break;
            case 5:
                wv = "nick";
                break;
        }
        return wv;
    }
    public static void setConfig(String setting, String value)
    {
        if (setting_java == null) { loadConfig(); }
        try {
            FileWriter fw = new FileWriter(String.format("%s/wolimc.cfg", getCwd()));
            int settIndex = getSettingLine(setting);
            for (int i = 0; i <= 5; i++)
            {
                if (i != settIndex && i == 0 && settIndex != 0)
                { fw.write("This is WoliMC config file\n"); }
                if (i != settIndex && i == 1 && settIndex != 1)
                { fw.write(String.format("%s\n", setting_java)); }
                if (i != settIndex && i == 2 && settIndex != 2)
                { fw.write(String.format("%s\n", setting_mem)); }
                if (i != settIndex && i == 3 && settIndex != 3)
                { fw.write(String.format("%s\n", setting_meta)); }
                if (i != settIndex && i == 4 && settIndex != 4)
                { fw.write(String.format("%s\n", setting_selinst)); }
                if (i != settIndex && i == 5 && settIndex != 5)
                { fw.write(String.format("%s\n", setting_nick)); }
                if (i == settIndex)
                {
                    fw.write(String.format("%s\n", value));
                }
            }
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static boolean int2bool(int num)
    {
        if (num == 1) { return true; } else { return false; }
    }
    public static int bool2int(boolean bool)
    {
        if (bool == true) { return 1; } else { return 0; }
    }
    public static void setKat(int value)
    {
        try {
            FileWriter fw = new FileWriter(String.format("%s/kat", getCwd()));
            fw.write(String.format("%d\n", value));
            kat_kommander = value;
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static String getLilypadId()
    {
        try {
            FileReader fr = new FileReader(String.format("%s/v3_act", getCwd()));
            BufferedReader br = new BufferedReader(fr);
            return br.readLine();
        } catch (IOException ioe) { System.out.println(ioe); return null; }
    }
    public static void setLilypadId(String qacode)
    {
        try {
            FileWriter fw = new FileWriter(String.format("%s/v3_act", getCwd()));
            fw.write(qacode);
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
}
