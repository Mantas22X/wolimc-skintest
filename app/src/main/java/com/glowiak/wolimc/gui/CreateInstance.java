package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;

import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.instance.Instance;

interface SCI
{
    JFrame w = new JFrame("Add Instance");
    JButton cb = new JButton("Create");
    JLabel text_nm = new JLabel("Name:");
    JTextField instNam = new JTextField();
    JLabel text_ver = new JLabel("Version:");
    JComboBox sel_ver = new JComboBox();
    JLabel text_class = new JLabel("Main Class:");
    JTextField tfmc = new JTextField();
    JLabel classWarn = new JLabel("In 1.6+ change main class to 'net.minecraft.client.main.Main'");
}

public class CreateInstance implements SCI
{
    public static void run()
    {
        draw(450, 200);
        instNam.setText("");
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        try {
            if (!new File(String.format("%s/version_names.txt", Config.getCwd())).exists())
            { Version.updateVersionManifest(); }
            
            FileReader fr = new FileReader(String.format("%s/version_names.txt", Config.getCwd()));
            BufferedReader br = new BufferedReader(fr);
            
            String s;
            while ((s = br.readLine()) != null)
            {
                sel_ver.addItem(s);
            }
            
            br.close();
            fr.close();
        } catch (IOException ioe) { System.out.println(ioe); }
        cb.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Instance.createInstance(instNam.getText(), sel_ver.getSelectedItem().toString());
                Instance.setProperty(instNam.getText(), "main-class", tfmc.getText());
                Config.setConfig("selinst", instNam.getText());
                w.dispose();
            }
        });
    }
    public static void draw(int width, int height)
    {
        w.setSize(width, height);
        
        cb.setBounds(width - 116, height - 55, 100, 25);
        text_nm.setBounds(10, 25, 100, 25);
        instNam.setBounds(100, 25, width - 100, 25);
        text_ver.setBounds(10, 50, 100, 25);
        sel_ver.setBounds(100, 50, width - 200, 25);
        text_class.setBounds(10, 75, 100, 25);
        tfmc.setBounds(100, 75, width - 100, 25);
        classWarn.setBounds(5, height - 85, width - 5, 25);
        
        tfmc.setText(Version.mainClass);
        
        w.add(cb);
        w.add(text_nm);
        w.add(instNam);
        w.add(text_ver);
        w.add(sel_ver);
        w.add(text_class);
        w.add(tfmc);
        w.add(classWarn);
        
        w.setLayout(null);
        w.setVisible(true);
    }
}
