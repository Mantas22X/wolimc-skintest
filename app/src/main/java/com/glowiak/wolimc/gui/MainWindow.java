package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.JCheckBox;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.AbstractButton;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.gui.SettingsWindow;
import com.glowiak.wolimc.WoliMC;
import com.glowiak.wolimc.gui.CreateInstance;
import com.glowiak.wolimc.launch.Classpath;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.launch.Launch;
import com.glowiak.wolimc.gui.InstanceOptions;
import com.glowiak.wolimc.launch.Assets;

interface WMB
{
    JFrame w = new JFrame(String.format("WoliMC - Version %d.%d.%d-%s", Config.WoliMC_VERSION_MAJOR, Config.WoliMC_VERSION_MINOR, Config.WoliMC_VERSION_HOTFIX, Config.WoliMC_CODE_BRANCH));
    JButton addInst = new JButton("Add Instance");
    JButton mcf = new JButton("Instances Folder");
    JButton setts = new JButton("Settings");
    JButton accb = new JButton("Accounts");
    JLabel nickl = new JLabel("");
    JCheckBox katk = new JCheckBox("Kat Kommander");
    JLabel TheKatKommander = new JLabel("");
    JRadioButton[] rbs = new JRadioButton[50];
    ButtonGroup bg = new ButtonGroup();
    JButton inst_launch = new JButton("Launch");
    JButton inst_setts = new JButton("Options");
    JButton inst_mcdir = new JButton("MC Dir");
    JButton inst_worlds = new JButton("Worlds");
    JButton inst_mods = new JButton("Mods");
}

public class MainWindow implements WMB
{
    public static void run()
    {
        Config.loadConfig();
        katk.setSelected(Config.int2bool(Config.kat_kommander));
        draw(900, 600);
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        setts.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                SettingsWindow.run();
            }
        });
        katk.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent e)
            {
                Config.kat_kommander = Config.bool2int(katk.isSelected());
                Config.setKat(Config.bool2int(katk.isSelected()));
                displayKatKommander();
            }
        });
        addInst.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) { CreateInstance.run(); }
        });
        mcf.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try {
                    if (Classpath.getOS() == "linux")
                    {
                        Runtime.getRuntime().exec(String.format("/usr/bin/env xdg-open %s/instances", Config.getCwd()));
                    } else if (Classpath.getOS() == "windows")
                    {
                        Runtime.getRuntime().exec(String.format("C:\\WINDOWS\\EXPLORER.EXE %s\\instances", Config.getCwd()));
                    }
                } catch (IOException ioe) { System.out.println(ioe); }
            }
        });
        inst_launch.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Classpath.fetchJars(Instance.getSelected());
                Assets.fetchAssets(Instance.getSelected());
                Launch.startInstance(Instance.getSelected());
            }
        });
        inst_mcdir.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if (!new File(String.format("%s/instances/%s/.minecraft", Config.getCwd(), Instance.getSelected())).exists())
                { new File(String.format("%s/instances/%s/.minecraft", Config.getCwd(), Instance.getSelected())).mkdir(); }
                
                try {
                    if (Classpath.getOS() == "linux")
                    {
                        Runtime.getRuntime().exec(String.format("/usr/bin/env xdg-open %s/instances/%s/.minecraft", Config.getCwd(), Instance.getSelected()));
                    } else if (Classpath.getOS() == "windows")
                    {
                        Runtime.getRuntime().exec(String.format("C:\\WINDOWS\\EXPLORER.EXE %s\\instances\\%s\\.minecraft", Config.getCwd(), Instance.getSelected()));
                    }
                } catch (IOException ioe) { System.out.println(ioe); }
            }
        });
        inst_setts.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                InstanceOptions.run();
            }
        });
    }
    public static void draw(int width, int height)
    {
        Config.loadConfig();
        w.setSize(width, height);
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        int katx = width/2 - 100;
        int katy = height - 420;
        
        addInst.setBounds(1, 1, 135, 25);
        mcf.setBounds(135, 1, 160, 25);
        setts.setBounds(135 + 160, 1, 100, 25);
        accb.setBounds(width - 115, 1, 100, 25);
        nickl.setBounds(10, height - 55, 100, 25);
        katk.setBounds(width - 150, height - 55, 150, 25);
        TheKatKommander.setBounds(katx, katy, width - katx, 450);
        TheKatKommander.setIcon(new ImageIcon(new WoliMC().getClass().getResource("/kat_kommander.png")));
        inst_launch.setBounds(width - 105, 100, 100, 25);
        inst_setts.setBounds(width - 105, 125, 100, 25);
        inst_mcdir.setBounds(width - 105, 150, 100, 25);
        inst_worlds.setBounds(width - 105, 175, 100, 25);
        inst_mods.setBounds(width - 105, 200, 100, 25);
        
        nickl.setText(Config.setting_nick);
        
        int base_x = 5;
        int base_y = 50;
        
        if (!new File(String.format("%s/instances/InstancesList", Config.getCwd())).exists())
        {
            try {
                new File(String.format("%s/instances/InstancesList", Config.getCwd())).createNewFile();
            } catch (IOException ioe) { System.out.println(ioe); }
        }
        
        for (int i = 0; i < Instance.getInstanceList().size(); i++)
        {
            String ii = Instance.getInstanceList().get(i);
            String ij = Instance.getSelected();
            boolean ifSelected;
            if (ii.contains(ij) || ij.contains(ii))
            { ifSelected = true; } else { ifSelected = false; }
            Config.loadConfig();
            rbs[i] = new JRadioButton(ii, ifSelected);
            rbs[i].setBounds(base_x, base_y, 150, 25);
            bg.add(rbs[i]);
            rbs[i].addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    Config.loadConfig();
                    Config.setConfig("selinst", getSelectedButtonText(bg));
                }
            });
            w.add(rbs[i]);
            
            base_x += 150;
            if (base_x >= width) { base_x = 10; base_y += 25; }
        }
        
        w.add(addInst);
        w.add(mcf);
        w.add(setts);
        w.add(accb);
        w.add(nickl);
        w.add(katk);
        w.add(TheKatKommander);
        w.add(inst_launch);
        w.add(inst_setts);
        w.add(inst_mcdir);
        w.add(inst_worlds);
        w.add(inst_mods);
        
        displayKatKommander();
        
        w.setLayout(null);
        w.setVisible(true);
    }
    public static void displayKatKommander()
    {
        if (Config.kat_kommander == 1) {
            TheKatKommander.setVisible(true);
            w.repaint();
        } else {
            TheKatKommander.setVisible(false);
            w.repaint();
        }
    }
    public static String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
}
