package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.glowiak.wolimc.config.Config;

interface SWB
{
    JFrame w = new JFrame("Settings");
    JButton applyb = new JButton("Apply");
    JLabel text_javap = new JLabel("Java path:");
    JTextField tf = new JTextField();
    JLabel text_mem = new JLabel("Memory:");
    JTextField tfm = new JTextField();
    JLabel text_ms = new JLabel("Metaserver:");
    JTextField tfms = new JTextField();
    JLabel text_nick = new JLabel("Nick:");
    JTextField tfn = new JTextField();
    JButton selj = new JButton("Browse");
    JLabel text_code = new JLabel("Lilypad QA ID:");
    JTextField tfc = new JTextField();
}

public class SettingsWindow implements SWB
{
    public static void run()
    {
        draw(400,250);
        
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        
        applyb.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Config.loadConfig();
                
                try {
                    FileWriter fw = new FileWriter(String.format("%s/wolimc.cfg", Config.getCwd()));
                    
                    fw.write("This is WoliMC config file\n");
                    fw.write(String.format("%s\n", tf.getText()));
                    fw.write(String.format("%s\n", tfm.getText()));
                    fw.write(String.format("%s\n", tfms.getText()));
                    fw.write(String.format("%s\n", Config.setting_selinst));
                    fw.write(String.format("%s\n", tfn.getText()));
                    
                    fw.close();
                    Config.setLilypadId(tfc.getText());
                } catch (IOException ioe) { System.out.println(ioe); }
                
                Config.loadConfig();
                
                w.dispose();
            }
        });
        selj.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser jfc = new JFileChooser();
                int oc = jfc.showOpenDialog(SWB.w);
                if (oc == JFileChooser.APPROVE_OPTION)
                {
                    tf.setText(jfc.getSelectedFile().getPath());
                }
            }
        });
    }
    public static void draw(int width, int height)
    {
        Config.loadConfig();
        w.setSize(width, height);
        
        applyb.setBounds(width - 114, height - 55, 100, 25);
        text_javap.setBounds(10, 25, 100, 25);
        tf.setBounds(110, 25, width - 210, 25);
        text_mem.setBounds(10, 50, 100, 25);
        tfm.setBounds(110, 50, width - 100, 25);
        text_ms.setBounds(10, 75, 100, 25);
        tfms.setBounds(110, 75, width - 100, 25);
        text_nick.setBounds(10, 100, 100, 25);
        tfn.setBounds(110, 100, width - 100, 25);
        selj.setBounds(width - 100, 25, 100, 25);
        text_code.setBounds(10, 125, 100, 25);
        tfc.setBounds(110, 125, width - 100, 25);
        
        tf.setText(Config.setting_java);
        tfm.setText(Config.setting_mem);
        tfms.setText(Config.setting_meta);
        tfn.setText(Config.setting_nick);
        tfc.setText(Config.getLilypadId());
        
        w.add(applyb);
        w.add(text_javap);
        w.add(tf);
        w.add(text_mem);
        w.add(tfm);
        w.add(text_ms);
        w.add(tfms);
        w.add(text_nick);
        w.add(tfn);
        w.add(selj);
        w.add(text_code);
        w.add(tfc);
        
        w.setLayout(null);
        w.setVisible(true);
    }
}
