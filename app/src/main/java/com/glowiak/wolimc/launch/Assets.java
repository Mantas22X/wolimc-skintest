package com.glowiak.wolimc.launch;

import com.glowiak.wolimc.DownloadFile;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.launch.Version;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Assets
{
    public static String RSC_BASE = "https://resources.download.minecraft.net";
    
    public static void fetchAssets(String instance)
    {
        if (!new File(String.format("%s/objects.txt", Config.getCwd())).exists()) { Version.updateVersionManifest(); }
        try {
            new File(String.format("%s/assets", Config.getCwd())).mkdir();
            new File(String.format("%s/assets/%s", Config.getCwd(), instance)).mkdir();
            new File(String.format("%s/assets/%s/objects", Config.getCwd(), instance)).mkdir();
            new File(String.format("%s/assets/%s/indexes", Config.getCwd(), instance)).mkdir();
            FileReader fr = new FileReader(String.format("%s/objects.txt", Config.getCwd()));
            BufferedReader br = new BufferedReader(fr);
            String s = null;
            String objdir = null;
            while ((s = br.readLine()) != null)
            {
                objdir = s.substring(0, 2);
                if (!new File(String.format("%s/assets/%s/objects/%s/%s", Config.getCwd(), instance, objdir, s)).exists())
                {
                    new File(String.format("%s/assets/%s/objects/%s", Config.getCwd(), instance, objdir)).mkdir();
                    DownloadFile.fetch(String.format("%s/%s/%s", RSC_BASE, objdir, s), String.format("%s/assets/%s/objects/%s/%s", Config.getCwd(), instance, objdir, s));
                }
            }
            if (!new File(String.format("%s/assets/%s/indexes/%s.json", Config.getCwd(), instance, Instance.getVersion(instance))).exists())
            {
                DownloadFile.fetch(String.format("%s/index.json", Config.setting_meta), String.format("%s/assets/%s/indexes/%s.json", Config.getCwd(), instance, Instance.getVersion(instance)));
            }
        } catch (IOException ioe) { System.out.println(ioe); }
    }
}
