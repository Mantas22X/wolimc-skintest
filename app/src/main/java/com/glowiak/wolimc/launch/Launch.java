package com.glowiak.wolimc.launch;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.launch.Classpath;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.instance.Instance;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;

public class Launch
{
    public static void startInstance(String instance)
    {
        Config.loadConfig();
        String version = Instance.getVersion(instance);
        String java = Instance.getJava(instance);
        String memory = Instance.getMemory(instance);
        String nick = Instance.getNick(instance);
        String mainClass = Instance.getMainClass(instance);
        
        System.out.println("Extracting natives...");
        Classpath.extractNatives(instance);
        
        // a1.0.16.05 patches
        if (Instance.getVersion(instance).contains("a1.0.16.05-preview") || "a1.0.16.05-preview".contains(Instance.getVersion(instance))) {
            if (new File(String.format("%s/preview_data", Config.getCwd())).exists()) // for some reason, the .05 files are
            { new File(String.format("%s/preview_data", Config.getCwd())).delete(); } // generating in launcher rootdir instead of .minecraft
        }
        
        String doExec;
        if (Instance.getJvmArgs(instance) != null) {
            doExec = String.format("%s -Xmn%s -Xmx%s -XX:+UseConcMarkSweepGC -Dfile.encoding=UTF-8 -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Djava.library.path=%s/instances/%s/natives -Dminecraft.launcher.brand=WoliMC -Dminecraft.launcher.version=%s -cp %s%s %s %s %s --username %s --version %s --userProperties {} --gameDir %s --assetsDir %s --assetIndex %s --accessToken 12 --userType legacy", java, memory, memory, Config.getCwd(), instance, String.format("%d.%d.%d", Config.WoliMC_VERSION_MAJOR, Config.WoliMC_VERSION_MINOR, Config.WoliMC_VERSION_HOTFIX), Classpath.build_classpath(), String.format("%s/instances/%s/minecraft.jar", Config.getCwd(), instance), Instance.getJvmArgs(instance), mainClass, nick, nick, version, String.format("%s/instances/%s/.minecraft", Config.getCwd(), instance), String.format("%s/assets/%s", Config.getCwd(), instance), version);
        } else {
            doExec = String.format("%s -Xmn%s -Xmx%s -XX:+UseConcMarkSweepGC -Dfile.encoding=UTF-8 -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Djava.library.path=%s/instances/%s/natives -Dminecraft.launcher.brand=WoliMC -Dminecraft.launcher.version=%s -cp %s%s %s %s --username %s --version %s --userProperties {} --gameDir %s --assetsDir %s --assetIndex %s --accessToken 12 --userType legacy", java, memory, memory, Config.getCwd(), instance,String.format("%d.%d.%d", Config.WoliMC_VERSION_MAJOR, Config.WoliMC_VERSION_MINOR, Config.WoliMC_VERSION_HOTFIX), Classpath.build_classpath(), String.format("%s/instances/%s/minecraft.jar", Config.getCwd(), instance), mainClass, nick, nick, version, String.format("%s/instances/%s/.minecraft", Config.getCwd(), instance), String.format("%s/assets/%s", Config.getCwd(), instance), version);
        }
        
        System.out.println(doExec);
        
        try {
            Process p = Runtime.getRuntime().exec(doExec);
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
 
            reader.close();
        }
        catch (IOException ioe) { System.out.println(ioe); }
    }
}
